// =========================== Required and fixed Input ===========================
var jsonfile = require('jsonfile');
var request = require('request');

// =========================== Input setup ===========================
const SENSOR_API_FINAL_URL = '/Datastreams';
// const SENSOR_API_BASE_URL = 'http://localhost:8080/SensorThingsService/v1.0';
// const SENSOR_API_BASE_URL = 'http://gisstudio.hft-stuttgart.de/FROST-icity/v1.0';
const SENSOR_API_BASE_URL = 'http://193.196.138.56:8080/frost-agrothermal-historical/v1.0';


// ============================ Define Variable =========================
// var datastreamBody = [];
var DS_Name;
var DS_Description;
var DS_obType;
var UoM_Name;
var UoM_Symbol;
var UoM_Definition;
var obsprop_id;
var s_id;
var CoreDS_Name;
var startingDS = 1;
var maxDatastream = 35;
var METHOD_http = 'POST'
var datastreamsArray = ["AA_LI_T1", "AA_LI_T2", "AA_LI_T3", "AA_LI_T4", "AA_RE_T1", "AA_RE_T2", "AA_RE_T3", "AA_RE_T4", "BB_LI_T1", "BB_LI_T2", "BB_LI_T3", "BB_LI_T4", "BB_RE_T1","BB_RE_T2", "BB_RE_T3", "BB_RE_T4", "CC_LI_T1", "CC_LI_T2", "CC_LI_T3", "CC_LI_T4", "CC_RE_T1", "CC_RE_T2", "CC_RE_T4", "DD_T4", "DD_T1", "DD_T2", "DD_T3", "BB_GW", "CC_GW", "AA_LI_H2", "AA_RE_H1", "CC_LI_H1", "CC_RE_H2", "AA_KR", "BB_KR"]
function Post_Sensorthings(st_id) {
    CoreDS_Name = "-"
    DS_Name = `${datastreamsArray[st_id - 1]}`
    DS_Description = `Datastreams collecting sensor data at ${datastreamsArray[st_id - 1]}`
    DS_obType = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement"
    if (st_id >= 28 && st_id <= 33) {
        UoM_Name = "percentage"
        UoM_Definition = "https://en.wikipedia.org/wiki/Percentage"
        UoM_Symbol = "%"
        obsprop_id = 2
    } else {
        UoM_Name = "Celsius"
        UoM_Definition = "https://en.wikipedia.org/wiki/Celsius"
        UoM_Symbol = "C"
        obsprop_id = 1
    }
    //* Format the datastream JSON body
    var datastreamBody = {
        "name": DS_Name,
        "description": DS_Description,
        "observationType": DS_obType,
        "unitOfMeasurement": {
            "name": UoM_Name,
            "symbol": UoM_Symbol,
            "definition": UoM_Definition
        },
        // "Thing":{"@iot.id":st_id},
        "Thing": {
            "@iot.id": 1
        },
        "ObservedProperty": {
            "@iot.id": obsprop_id
        },
        "Sensor": {
            "@iot.id": 1
        }
    };
    //console.log(JSON.stringify(datastreamBody));  */ command to check the datastream body

    //console.log(`Sending POST request of DataStream [ObsPropID: ${o_id}][ThingID:${st_id}][SensorID:${s_id}] `)
    postSTA(datastreamBody, st_id);
}

function postSTA(DS_Body, i) {
    let urlpost = SENSOR_API_BASE_URL + SENSOR_API_FINAL_URL
    if (METHOD_http == "PATCH") {
        urlpost = SENSOR_API_BASE_URL + SENSOR_API_FINAL_URL + `(${i})`
    }
    let headers = {
        'Content-Type': 'application/json'
    };
    let options = {
        url: urlpost,
        headers: headers,
        method: METHOD_http,
        body: JSON.stringify(DS_Body),
    }
    //console.log(item);

    request(options, function (error, httpResponse, body) {
        if (error || i == maxDatastream) {
            console.log(`Sending POST request of DataStream [${i}] successfully`);
            console.log(`>> All Job Complete`);
            return console.error(`show error >>` + error);
        }
        // If not error
        //console.log("-------------Post datastream to STA successful!-------------", body);
        console.log(`Sending POST request to ${urlpost}`);
        console.log(`Sending POST request of DataStream [${i}] => successfully`, body);
        //console.log(DS_Body[i]);
        Post_Sensorthings(i + 1)
    })
}
Post_Sensorthings(startingDS);