const fs = require('fs');
var jsonfile = require('jsonfile');

// Build CZML with the incoming Position data
var BuildCZML = function (pos) {
    var startTime = "2017-11-22T13:04:49Z";
    var endTime = "2017-11-22T15:00:00Z";
    //var pos = [0, 9.17761392, 48.7831606, 305.1999969, 1, 9.177607214, 48.78316504, 305.1999969];
    var result = [{
        "id": "document",
        "name": "SampleGarmin",
        "version": "1.0",
        "clock": {
            "interval": "2017-11-22T13:04:49Z/2017-11-22T15:00:00Z",
            "currentTime": "2017-11-22T13:04:49Z",
            "multiplier": 20,
            "range": "LOOP_STOP",
            "step": "SYSTEM_CLOCK_MULTIPLIER"
        }
    }, {
        "id": "ebike1",
        "name": "garmin",
        "availability": "2017-11-22T13:04:49Z/2017-11-22T15:00:00Z",
        "ellipsoid": {
            "radii": {
                "cartesian": [3.0, 3.0, 3.0]
            },
            "fill": true,
            "material": {
                "solidColor": {
                    "color": {
                        "rgba": [255, 0, 0, 255]
                    }
                }
            }
        },
        "path": {
            "show": [{
                "interval": "2017-11-22T13:04:49Z/2017-11-22T15:00:00Z",
                "boolean": true
            }
            ],
            "width": 1,
            "material": {
                "polylineOutline": {
                    "color": {
                        "rgba": [0, 255, 0, 255]
                    },
                    "outlineColor": {
                        "rgba": [0, 255, 0, 255]
                    },
                    "outlineWidth": 1
                }
            },
            "resolution": 1200,
            "leadTime": 0
        },
        "position": {
            "interpolationAlgorithm": "LAGRANGE",
            "interpolationDegree": 5,
            "epoch": startTime,
            "cartographicDegrees": pos
        }
    }];
    var file = 'czmlBuild.json';
    jsonfile.writeFile(file, result, function (err) {
        if (err) throw err;
        console.log('filesaved');
    })
}
BuildCZML();