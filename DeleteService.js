    // =========================== Require setup ===========================
    const fetch = require('node-fetch');
    var jsonfile = require('jsonfile');
    var request = require('request');

    // =========================== Input setup ===========================
    
    const SENSOR_API_BASE_URL = 'http://localhost:8080/SensorThingsService/v1.0';
    const SENSOR_API_FINAL_URL = '/ObservedProperties';
    var a = 3; //First IoT to delete
    var b = 18;//Last IoT to delete
    // ===================================================================
    function deleteAll() {
      for (let i = a; i < (b + 1); i++) {
        deleteSTA(i);
        }
  
    }
    function deleteSTA(i) {
        let headers = {'Content-Type': 'application/json'};
        let options = {
            url: SENSOR_API_BASE_URL + SENSOR_API_FINAL_URL + '(' + i + ')',
            headers: headers,
            method: 'DELETE',
            }
        request(options, function (error, httpResponse, body) {
            if (error) {
                return console.error('Delete data failed:', error);
              }
            // Print out the response body
                console.log('Delete data on STA successfully (number : ' + i + ')', body);
                
            })
    }
deleteAll();