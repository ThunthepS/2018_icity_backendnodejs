[{
        "Datastream": {
            "@iot.id": 1
        },
        "components": [
            "phenomenonTime",
            "result"
        ],
        "dataArray@iot.count": 2,
        "dataArray": [
            [
                "2010-12-23T10:20:00-0700",
                20
            ],
            [
                "2010-12-23T10:21:00-0700",
                30
            ]
        ]
    },
    {
        "Datastream": {
            "@iot.id": 2
        },
        "components": [
            "phenomenonTime",
            "result"
        ],
        "dataArray@iot.count": 2,
        "dataArray": [
            [
                "2010-12-23T10:20:00-0700",
                65
            ],
            [
                "2010-12-23T10:21:00-0700",
                60
            ]
        ]
    }
]