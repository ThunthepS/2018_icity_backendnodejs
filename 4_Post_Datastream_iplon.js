    // =========================== Required and fixed Input ===========================
    var jsonfile = require('jsonfile');
    var request = require('request');

    // =========================== Input setup ===========================
    const SENSOR_API_FINAL_URL = '/Datastreams';
    // const SENSOR_API_BASE_URL = 'http://localhost:8080/SensorThingsService/v1.0';
    // const SENSOR_API_BASE_URL = 'http://gisstudio.hft-stuttgart.de/FROST-icity/v1.0';
    const SENSOR_API_BASE_URL = 'http://193.196.138.56:8080/frost-hft-iplon/v1.0';
    
    // ============================ Define Variable =========================
    var datastreamBody = [];
    var DS_Name;
    var DS_Description;
    var DS_obType;
    var UoM_Name;
    var UoM_Symbol;
    var UoM_Definition;
    var s_id;
    var CoreDS_Name;
    var maxDatastream = 27;

    function Post_Sensorthings() {
                    CoreDS_Name = "-"
                    DS_Name = `test - Datastream`,
                        DS_Description = `Please make PUT request to replace this datastream`,
                        DS_obType = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement",
                        UoM_Name = "-",
                        UoM_Definition = "https://en.wikipedia.org/wiki/Percentage",
                        UoM_Symbol = "%"
                //* Format the datastream JSON body
                datastreamBody = {
                    "name": DS_Name,
                    "description": DS_Description,
                    "observationType": DS_obType,
                    "unitOfMeasurement": {
                        "name": UoM_Name,
                        "symbol": UoM_Symbol,
                        "definition": UoM_Definition
                    },
                    // "Thing":{"@iot.id":st_id},
                    "Thing": {
                        "@iot.id": 1
                    },
                    "ObservedProperty": {
                        "@iot.id": 1
                    },
                    "Sensor": {
                        "@iot.id": 1
                    }
                };
                //console.log(JSON.stringify(datastreamBody));  */ command to check the datastream body

                //console.log(`Sending POST request of DataStream [ObsPropID: ${o_id}][ThingID:${st_id}][SensorID:${s_id}] `)
        postSTA(datastreamBody, 0);
    }

    function postSTA(DS_Body, i) {
        let headers = {
            'Content-Type': 'application/json'
        };
        let options = {
            url: SENSOR_API_BASE_URL + SENSOR_API_FINAL_URL,
            headers: headers,
            method: 'POST',
            body: JSON.stringify(DS_Body),
        }
        //console.log(item);

        request(options, function (error, httpResponse, body) {
            if (error || i == maxDatastream-1) {
                console.log(`Sending POST request of DataStream [${i}] successfully`);
                console.log(`>> All Job Complete`);
                return console.error(`show error >>` + error);
            }
            // If not error
            //console.log("-------------Post datastream to STA successful!-------------", body);
            console.log(`Sending POST request of DataStream [${i}] => successfully`, body);
            //console.log(DS_Body[i]);
            postSTA(DS_Body, i + 1)
        })
    }
    Post_Sensorthings();