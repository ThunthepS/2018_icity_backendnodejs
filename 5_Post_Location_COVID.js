    // =========================== Required and fixed Input ===========================
    var jsonfile = require('jsonfile');
    var request = require('request');

    // =========================== Input setup ===========================
    const SENSOR_API_FINAL_URL = '/Locations';

    // const SENSOR_API_BASE_URL = 'http://localhost:8080/SensorThingsService/v1.0';
    // const SENSOR_API_BASE_URL = 'http://gisstudio.hft-stuttgart.de/FROST-icity/v1.0';
    // const SENSOR_API_BASE_URL = 'http://gisstudio.hft-stuttgart.de/FROST-HFT/v1.0';
    const SENSOR_API_BASE_URL = 'http://covidsta.hft-stuttgart.de/server/v1.1';
var file = 'Data/STA_initialData_covid3/Locations.json'; //Local input file
    
var i = 2; //start Thing ID 1
var j = 187; //stop Thing ID 6
// ===================================================================
function Post_Location() {
    jsonfile.readFile(file, function (err, obj) {
        PostSTA_Location(obj, i);
    })
}

function PostSTA_Location(item, i) {
    let headers = {
        'Content-Type': 'application/json'
    };
    let options = {
        url: SENSOR_API_BASE_URL + SENSOR_API_FINAL_URL + '(' + i + ')',
        headers: headers,
        method: 'PATCH',
        body: JSON.stringify(item[i-1]),
    }
    request(options, function (error, httpResponse, body) {
        if (error || i == j) {
            return console.log('Post data to STA :' + i + ' >> All Job Complete');
        }
        // Print out the response body
        console.log('Post data to STA :' + i + ' successful!', body);
        PostSTA_Location(item, i + 1)
    })
}
Post_Location();