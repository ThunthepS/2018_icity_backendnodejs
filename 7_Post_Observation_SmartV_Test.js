// =========================== Required and fixed Input ===========================
var _ = require('lodash');
var request = require('request');
// const SENSOR_API_BASE_URL = 'http://localhost:8080/SensorThingsService/v1.0';
// const SENSOR_API_BASE_URL = 'http://gisstudio.hft-stuttgart.de/FROST-icity/v1.0';
const SENSOR_API_BASE_URL = 'http://gisstudio.hft-stuttgart.de/FROST-HFT/v1.0';
const SENSOR_API_FINAL_URL = '/Observations';
var isodate = require("isodate");
// =====================Set File Location==============
const fs = require('fs');

var execute = true;         //Default at false to show the result in terminal first (not POST yet)

function generateRequestfromJSON() {
    var dataTime = new Date().toISOString();
    // condition if(...) will check if the incoming JSON has the field of data or not
    // variable id matches the datastream ID in STA
    // variable dataStr_[Entity's name] constructs the POST request Which will sent through function postSTA(...) 

    var id = 5000;
    var dataStr_fuelLevel = {
        "phenomenonTime": dataTime,
        "resultTime": dataTime,
        "result": 1,
        "Datastream": { "@iot.id": id }
    };
    if (execute) {
        postSTA(dataStr_fuelLevel, 0);
    } else {
        console.log(`DataStream Body fuelLevel: ` + JSON.stringify(dataStr_fuelLevel));
        console.log('---------------------------------------------------')
    };

}
//Build the Observation Body
//PhenomenonTime is the time instant or period of when the Observation happens

// check some variable here
//console.log('...' + checkAltitude);
//console.log('================================');


// * Function to check the ebikeVIN id here
function executeSTid(VIN) {
    if (VIN == "eBike20131126003c") { var id = 1; }
    else if (VIN == "eBike201209280029") { var id = 2; }
    else if (VIN == "eBike20131107003d") { var id = 3; }
    else if (VIN == "eBike201311270017") { var id = 4; }
    else { var id = 0; }
    return id;
}

// * Function to make POST request to the STA
function postSTA(item, i) {
    let headers = { 'Content-Type': 'application/json' };
    let options = {
        url: SENSOR_API_BASE_URL + SENSOR_API_FINAL_URL,
        headers: headers,
        method: 'POST',
        body: JSON.stringify(item),
    }
    //console.log(item);
    request(options, function (error, httpResponse, body) {
        if (error) {
            return console.error(`Post data failed:`, error);
        }
        // Print out the response body
        console.log(`Post datastream [${i}] to STA successfully!!`, body);
    })
}
generateRequestfromJSON();