    // =========================== Required and fixed Input ===========================
    var jsonfile = require('jsonfile');
    var request = require('request');

    // =========================== Input setup ===========================
    const SENSOR_API_FINAL_URL = '/Datastreams';
// const SENSOR_API_BASE_URL = 'http://localhost:8080/SensorThingsService/v1.0';
// const SENSOR_API_BASE_URL = 'http://193.196.138.56:8080/frost-hft-sensors/v1.0';
// const SENSOR_API_BASE_URL = 'http://193.196.138.56:8080/frost-citythings-musi/v1.0';
const SENSOR_API_BASE_URL = 'http://193.196.138.56/musithings/v1.0';

// const SENSOR_API_BASE_URL = 'http://193.196.138.56:8080/frost-hft-iplon/v1.0';
//const SENSOR_API_BASE_URL = 'http://gisstudio.hft-stuttgart.de/FROST-icity/v1.0';   

    // Set variable depend on the object in SensorThings Service
    var IdObsProp_First = 1; //
    var IdObsProp_Last = 11; //
    var IdThings_First = 2; //
    var IdThings_Last = 125; //
    // ============================ Define Variable =========================
    var datastreamBody = [];
    var DS_Name;
    var DS_Description;
    var DS_obType;
    var UoM_Name;
    var UoM_Symbol;
    var UoM_Definition;
    var s_id;
    var CoreDS_Name;
    var count = 0;

    function Post_Sensorthings() {
        //loop through all STA services
        //* o_id --> ObservedProperties
        //* st_id --> Things and Sensors ID
        for (let st_id = IdThings_First; st_id <= IdThings_Last; st_id++) { /*Whole range 1 to 4*/
            for (let o_id = IdObsProp_First; o_id <= IdObsProp_Last; o_id++) { /*Whole range 1 to 16*/

                // match the IoT id of <Things and Sensors>
                if (o_id == 1) {
                    CoreDS_Name = "PV potential nominal power"
                    DS_Name = `${CoreDS_Name}`,
                        DS_Description = `Datastream for recording ${CoreDS_Name}`,
                        DS_obType = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement",
                        UoM_Name = "kilowatt peak",
                        UoM_Definition = "https://en.wikipedia.org/wiki/Nominal_power_(photovoltaic)",
                        UoM_Symbol = "kWp"
                }
                if (o_id == 2) {
                    CoreDS_Name = "PV potential yield"
                    DS_Name = `${CoreDS_Name}`,
                        DS_Description = `Datastream for recording ${CoreDS_Name}`,
                        DS_obType = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement",
                        UoM_Name = "MWh/a",
                        UoM_Definition = "-",
                        UoM_Symbol = "MWh/a"
                }
                if (o_id == 3) {
                    CoreDS_Name = "PV specific yield"
                    DS_Name = `${CoreDS_Name}`,
                        DS_Description = `Datastream for recording ${CoreDS_Name}`,
                        DS_obType = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement",
                        UoM_Name = "kWh/(kWp.a)",
                        UoM_Definition = "-",
                        UoM_Symbol = "kWh/(kWp.a)"
                }
                if (o_id == 4) {
                    CoreDS_Name = "Total investment"
                    DS_Name = `${CoreDS_Name}`,
                        DS_Description = `Datastream for recording ${CoreDS_Name}`,
                        DS_obType = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement",
                        UoM_Name = "EURO",
                        UoM_Definition = "https://en.wikipedia.org/wiki/Euro",
                        UoM_Symbol = "EURO"
                }
                if (o_id == 5) {
                    CoreDS_Name = "LCOE"
                    DS_Name = `${CoreDS_Name}`,
                        DS_Description = `Datastream for recording ${CoreDS_Name}`,
                        DS_obType = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement",
                        UoM_Name = "cE/kWh",
                        UoM_Definition = "-",
                        UoM_Symbol = "cE/kWh"
                }
                if (o_id == 6) {
                    CoreDS_Name = "Net present value"
                    DS_Name = `${CoreDS_Name}`,
                        DS_Description = `Datastream for recording ${CoreDS_Name}`,
                        DS_obType = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement",
                        UoM_Name = "EURO",
                        UoM_Definition = "https://en.wikipedia.org/wiki/Euro",
                        UoM_Symbol = "EURO"
                }
                if (o_id == 7) {
                    CoreDS_Name = "Internal rate of return"
                    DS_Name = `${CoreDS_Name}`,
                        DS_Description = `Datastream for recording ${CoreDS_Name}`,
                        DS_obType = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement",
                        UoM_Name = "percentage",
                        UoM_Definition = "https://en.wikipedia.org/wiki/Percentage",
                        UoM_Symbol = "%"
                }
                if (o_id == 8) {
                    CoreDS_Name = "Payback period"
                    DS_Name = `${CoreDS_Name}`,
                        DS_Description = `Datastream for recording ${CoreDS_Name}`,
                        DS_obType = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement",
                        UoM_Name = "a",
                        UoM_Definition = "-",
                        UoM_Symbol = "a"
                }
                if (o_id == 9) {
                    CoreDS_Name = "Discounted payback period"
                    DS_Name = `${CoreDS_Name}`,
                        DS_Description = `Datastream for recording ${CoreDS_Name}`,
                        DS_obType = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement",
                        UoM_Name = "a",
                        UoM_Definition = "-",
                        UoM_Symbol = "a"
                }
                if (o_id == 10) {
                    CoreDS_Name = "Financial feasibility"
                    DS_Name = `${CoreDS_Name}`,
                        DS_Description = `Datastream for recording ${CoreDS_Name}`,
                        DS_obType = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement",
                        UoM_Name = "-",
                        UoM_Definition = "-",
                        UoM_Symbol = "-"
                }
                if (o_id == 11) {
                    CoreDS_Name = "Maintenance costs"
                    DS_Name = `${CoreDS_Name}`,
                        DS_Description = `Datastream for recording ${CoreDS_Name}`,
                        DS_obType = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement",
                        UoM_Name = "EURO/a",
                        UoM_Definition = "-",
                        UoM_Symbol = "EURO/a"
                }

                //* Format the datastream JSON body
                datastreamBody[count] = {
                    "name": DS_Name,
                    "description": DS_Description,
                    "observationType": DS_obType,
                    "unitOfMeasurement": {
                        "name": UoM_Name,
                        "symbol": UoM_Symbol,
                        "definition": UoM_Definition
                    },
                    // "Thing":{"@iot.id":st_id},
                    "Thing": {
                        "@iot.id": st_id
                    },
                    "ObservedProperty": {
                        "@iot.id": o_id
                    },
                    "Sensor": {
                        "@iot.id": 1
                    }
                };
                //console.log(JSON.stringify(datastreamBody));  */ command to check the datastream body
                //console.log(`Sending POST request of DataStream [ObsPropID: ${o_id}][ThingID:${st_id}][SensorID:${s_id}] `)
                count++;
            }
        }
        postSTA(datastreamBody, 0);
    }

    function postSTA(DS_Body, i) {
        //var x = 81 + i;
        let headers = {
            'Content-Type': 'application/json'
        };
        let options = {
            url: SENSOR_API_BASE_URL + SENSOR_API_FINAL_URL,
            headers: headers,
            method: 'POST',
            body: JSON.stringify(DS_Body[i]),
        }
        //console.log(item);

        request(options, function (error, httpResponse, body) {
            if (error || i == (DS_Body.length - 1)) {
                console.log(`Sending POST request of DataStream [${i}] successfully`);
                console.log(`>> All Job Complete`);
                return console.error(`show error >>` + error);
            }
            // If not error
            //console.log("-------------Post datastream to STA successful!-------------", body);
            console.log(`Sending POST request of DataStream [${i}] => successfully`, body);
            //console.log(DS_Body[i]);
            postSTA(DS_Body, i + 1)

        })
    }
    Post_Sensorthings();