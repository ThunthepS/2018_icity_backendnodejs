// ==========================================================================
//                     Required and fixed Input 
// ==========================================================================
var jsonfile = require('jsonfile'); // JS package to easily read JSON package.
var request = require('request'); // request package use for making http request.
// ==========================================================================
//                           Input setup 
// ==========================================================================
const SENSOR_API_BASE_URL = 'http://localhost:8080/SensorThingsService/v1.0';
const SENSOR_API_FINAL_URL = '';   // '' /Things and /Sensors or /ObservedProperties
var file = '';  //input File Path here (ex. 'Data/STA_initialData/Sensors_ebike.json')
var n = 0;      // Starting reading object from json file.
var object;     // Variable for storing JSON object. 
// ==========================================================================
function Post_Sensor() {
    jsonfile.readFile(file, function (err, obj) {
        PostSTA_Sensor(obj, n);
    })
}
// =========================================================================
function PostSTA_Sensor(item, i) {
    let headers = { 'Content-Type': 'application/json' };
    let options = {
        url: SENSOR_API_BASE_URL + SENSOR_API_FINAL_URL,
        headers: headers,
        method: 'POST',
        body: JSON.stringify(item[i]),
    }
    request(options, function (error, httpResponse, body) {
        // looping until reaching the last JSON object 
        // object [0] ==> object [length -1]
        if (error || i == (item.length - 1)) {
            return console.log('Post data to STA :' + i + ' >> All Job Complete');
        }
        // Print out the response body
        console.log('Post data to STA :' + i + ' successful!', body);
        PostSTA_Sensor(item, i + 1)
    })
}
// ========================================================================
//                  Activate the Post_Sensor Function 
// ========================================================================
Post_Sensor();
