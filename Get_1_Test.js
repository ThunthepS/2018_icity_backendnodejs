// =========================== Required and fixed Input ===========================
const fs = require('fs');
var _ = require('lodash');
var request = require('request');
var jsonfile = require('jsonfile');
var checkDS = require('./CheckDatastreamID');
const SENSOR_API_BASE_URL = 'http://localhost:8080/SensorThingsService/v1.0';
// Get Observation(1) with the Expand datastream
//const SENSOR_API_FINAL_URL = '/Observations(1)?$expand=Datastream';
// Get Fuel Level value of the ebike1 which is ordered by result-time with the first 3 values!!  
const SENSOR_API_FINAL_URL = '/Datastreams(72)/Observations?$select=result,resultTime&$orderby=resultTime&$top=20'
// --------------------------- ebike2
//const SENSOR_API_FINAL_URL = '/Datastreams(2)/Observations?$select=result,resultTime&$orderby=resultTime&$top=3'
// --------------------------- ebike3
//const SENSOR_API_FINAL_URL = '/Datastreams(3)/Observations?$select=result,resultTime&$orderby=resultTime&$top=3'
// --------------------------- ebike4---**** Data Array output version!!!
//const SENSOR_API_FINAL_URL = '/Datastreams(4)/Observations?$select=result,resultTime&$orderby=resultTime&$top=3&$resultFormat=dataArray'
// ---------------
//Get last value of the data steam 78
//http://localhost:8080/SensorThingsService/v1.0/Datastreams(78)/Observations?$orderby=resultTime%20desc&$top=1
// ================================================================================

function getSTA() {
    let headers = { 'Content-Type': 'application/json' };
    let options = {
        url: SENSOR_API_BASE_URL + SENSOR_API_FINAL_URL,
        headers: headers,
        method: 'GET',
        //body: JSON.stringify(item),
    }
    //console.log(item);
    request(options, function (error, httpResponse, body) {
        if (error) {
            return console.error(`Get data failed:`, error);
        }
        // Print out the response body

        //console.log(`Show response body ==>`, body);
        test = body;
        jsonBody = JSON.parse(body);
        // jsonfile.readFile(body, function (err, obj) {
        // jsonBody = obj;
        // })
    })
}
var test;
var jsonBody;
var timeArray = [];
var resultArray = [];
var linear = require('everpolate').linear;
var linearResult;
var rangeSec, rangeMin, rangeHour;
var ArraySize;
var startTime, endTime, startTimeInt, endTimeInt;
var idx;
// Get Observation
getSTA();
setTimeout(function cb() {
    //Try loop the jsonBody value and push data of time and value into the new array
    ArraySize = _.size(jsonBody.value);
    for (let i = 0; i < ArraySize /*size of the jsonBody*/; i++) {
        timeArray.push(Date.parse(jsonBody.value[i].resultTime));
        resultArray.push(jsonBody.value[i].result);
    }
    //Result Time (First and last) 
    startTime = jsonBody.value[0].resultTime;
    startTimeInt = Date.parse(startTime);
    endTime = jsonBody.value[ArraySize - 1].resultTime;
    endTimeInt = Date.parse(endTime);
    //Create Range
    rangeSec = _.range(1511226157000, 1511226170000, 1000);
    rangeMin = _.range(startTimeInt, endTimeInt, 1000 * 60); //60 Seconds
    rangeHour = _.range(startTimeInt, endTimeInt, 1000 * 3600);

    //Check ID from CheckDatastreamID.js
    checkDS.CheckDSid('batteryVoltageBike', '4', function (id) {
        idx = id;
    }
    );
    //Try Linear interpolation every mins ==> Use raneMin
    setTimeout(function cb() {
        linearResult = linear(rangeMin, timeArray, resultArray) //using everpolate library!!
    }, 500);

}, 1000);
// print result after all computation has done!!
setTimeout(function cb() {
    console.log('jsonBody Size : ' + ArraySize)
    console.log('time array : ' + timeArray);
    console.log('result array : ' + resultArray);
    console.log('RangeMin result : ' + rangeMin);
    console.log('RangeMin size : ' + _.size(rangeMin));
    console.log('RangeHour result : ' + rangeHour);
    console.log('fuel level in 1 min interval  by linear-interpolation : ' + linearResult);
    console.log('checkDSid : ' + idx);
}, 2000);
console.log('========Processing========');
//http://localhost:8080/SensorThingsService/v1.0/Datastreams(1)/Observations?$filter=hour(phenomenonTime) ge 2 and hour(phenomenonTime) le 10