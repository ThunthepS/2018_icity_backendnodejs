module.exports = {
  //var check = 'batteryVoltageBike';
  //var Thing = '2';
  CheckDSid: function (check, Thing, callb) {
    var parse = require('csv-parse');
    const fs = require('fs');
    var DataStreamID, out;
    fs.readFile('./Data/STA_initialData/STA_DataStructure.csv', function (err, output) {
      if (err) throw err;
      out = output;
      parse(out, { comment: '#' }, function (err, output) {
        //console.log(output[1][1]);
        for (let i = 0; i < output.length /*size of the csv -1*/; i++) {
          if (check == output[i][1] && Thing == output[i][2]) {
            DataStreamID = output[i][0];
            //console.log(DataStreamID);
            callb(DataStreamID);
          }
        }
      });
    });
  }
}
