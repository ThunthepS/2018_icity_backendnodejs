import json
def CleanSTRforJSON(input_str):
    result_str = str(input_str)
    result_str = result_str.replace("'", '"')
    return result_str

def ReplaceCommaWithDot(input_str):
    result_str = str(input_str)
    result_str = result_str.replace(",", '.')
    return result_str

with open('ExampleResult.json') as json_file:
    result = json.load(json_file)
    final_Things = []
    i = 0
    for building in result['data']:
        if i > 0 :
            print('Processing Building ID: ' + building['Building ID'] + '...')
            jsonbody_obs1 = { 
                "Datastream": { "@iot.id": 1 + (i-1)*11}, 
                "components": ["resultTime","phenomenonTime","result"],
                "dataArray": [[ "2020-08-01T00:00:00Z","2020-08-01T00:00:00Z", building['PV potential nominal power']]]
            }
            final_Things.append(jsonbody_obs1)

            jsonbody_obs2 = { 
                "Datastream": { "@iot.id": 2 + (i-1)*11}, 
                "components": ["resultTime","phenomenonTime","result"],
                "dataArray": [[ "2020-08-01T00:00:00Z","2020-08-01T00:00:00Z",ReplaceCommaWithDot(building['PV potential yield'])]]
            }
            final_Things.append(jsonbody_obs2)

            jsonbody_obs3 = { 
                "Datastream": { "@iot.id": 3 + (i-1)*11}, 
                "components": ["resultTime","phenomenonTime","result"],
                "dataArray": [[ "2020-08-01T00:00:00Z","2020-08-01T00:00:00Z",ReplaceCommaWithDot(building['PV specific yield'])]]
            }
            final_Things.append(jsonbody_obs3)

            jsonbody_obs4 = { 
                "Datastream": { "@iot.id": 4 + (i-1)*11}, 
                "components": ["resultTime","phenomenonTime","result"],
                "dataArray": [[ "2020-08-01T00:00:00Z","2020-08-01T00:00:00Z",ReplaceCommaWithDot(building['Total investment'])]]
            }
            final_Things.append(jsonbody_obs4)

            jsonbody_obs5 = { 
                "Datastream": { "@iot.id": 5 + (i-1)*11}, 
                "components": ["resultTime","phenomenonTime","result"],
                "dataArray": [[ "2020-08-01T00:00:00Z","2020-08-01T00:00:00Z",ReplaceCommaWithDot(building['LCOE'])]]
            }
            final_Things.append(jsonbody_obs5)

            jsonbody_obs6 = { 
                "Datastream": { "@iot.id": 6 + (i-1)*11}, 
                "components": ["resultTime","phenomenonTime","result"],
                "dataArray": [[ "2020-08-01T00:00:00Z","2020-08-01T00:00:00Z",ReplaceCommaWithDot(building['Net present value'])]]
            }
            final_Things.append(jsonbody_obs6)

            jsonbody_obs7 = { 
                "Datastream": { "@iot.id": 7 + (i-1)*11}, 
                "components": ["resultTime","phenomenonTime","result"],
                "dataArray": [[ "2020-08-01T00:00:00Z","2020-08-01T00:00:00Z",ReplaceCommaWithDot(building['Internal rate of return'])]]
            }
            final_Things.append(jsonbody_obs7)

            jsonbody_obs8 = { 
                "Datastream": { "@iot.id": 8 + (i-1)*11}, 
                "components": ["resultTime","phenomenonTime","result"],
                "dataArray": [[ "2020-08-01T00:00:00Z","2020-08-01T00:00:00Z",ReplaceCommaWithDot(building['Payback period'])]]
            }
            final_Things.append(jsonbody_obs8)

            jsonbody_obs9 = { 
                "Datastream": { "@iot.id": 9 + (i-1)*11}, 
                "components": ["resultTime","phenomenonTime","result"],
                "dataArray": [[ "2020-08-01T00:00:00Z","2020-08-01T00:00:00Z",ReplaceCommaWithDot(building['Discounted payback period'])]]
            }
            final_Things.append(jsonbody_obs9)

            jsonbody_obs10 = { 
                "Datastream": { "@iot.id": 10 + (i-1)*11}, 
                "components": ["resultTime","phenomenonTime","result"],
                "dataArray": [[ "2020-08-01T00:00:00Z","2020-08-01T00:00:00Z",ReplaceCommaWithDot(building['Financial feasibility'])]]
            }
            final_Things.append(jsonbody_obs10)

            jsonbody_obs11 = { 
                "Datastream": { "@iot.id": 11 + (i-1)*11}, 
                "components": ["resultTime","phenomenonTime","result"],
                "dataArray": [[ "2020-08-01T00:00:00Z","2020-08-01T00:00:00Z",ReplaceCommaWithDot(building['Maintenance costs'])]]
            }
            final_Things.append(jsonbody_obs11)


        i = i +1
    obs_out = "Observations_result.json"
    f = open(obs_out,"w")
    f.write(CleanSTRforJSON(final_Things))
    f.close()
    print('Processing Complete ...')
