import json
def CleanSTRforJSON(input_str):
    result_str = str(input_str)
    result_str = result_str.replace("'", '"')
    return result_str

with open('ExampleResult.json') as json_file:
    result = json.load(json_file)
    final_Things = []
    i = 0
    for building in result['data']:
        if i > 0 :
            print('Processing Things JSON of Building ID: ' + building['Building ID'] + '...')

            jsonbody = {
                "name": building['Building ID'],
                "description": f"A building in GW area {building['Building ID']}",
                "properties": {
                    "surface_gml_id": f"{building['BuildingSurface ID']}",
                    "building_gml_id": f"{building['Building ID']}",
                    "BuildingSurface_Hash": f"{building['BuildingSurface Hash']}",
                    "individual_or_multiple_buildings": "individual",
                    "area": "Gruenbuehl"
                }
            }
            final_Things.append(jsonbody)

        i = i +1
    obs_out = "Things_result.json"
    f = open(obs_out,"w")
    f.write(CleanSTRforJSON(final_Things))
    f.close()
    print(f"total {i} buildings had been processed")
    print('Job Complete ...')
