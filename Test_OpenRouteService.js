const apiK = '58d904a497c67e00015b45fc1512797ef30249a395e53c3b97a807e7';
//HBF
var fromPo = [9.181392, 48.783623];
//HFT
var toPo = [9.172445, 48.780299];
var fromToPo = fromPo[0] + '%2C' + fromPo[1]+'%7C' + toPo[0] + '%2C' + toPo[1]
//==================================
const SENSOR_API_BASE_URL = 'https://api.openrouteservice.org/directions?api_key=' + apiK;
const SENSOR_API_FINAL_URL =    '&coordinates='+fromToPo +'&profile=cycling-electric&geometry_format=geojson' + 
                                '&elevation=true' + '&instructions_format=html';
var request = require('request');
function TimeGetChecker() {
    let headers = { 'Content-Type': 'application/json' };
    let options = {
        url: SENSOR_API_BASE_URL + SENSOR_API_FINAL_URL,
        headers: headers,
        method: 'GET',
        time: true
    }
    request(options, function (error, httpResponse, body) {
        console.log('Request time in ms :', httpResponse.elapsedTime);
        console.log('Body :', body);
    })
}
TimeGetChecker();