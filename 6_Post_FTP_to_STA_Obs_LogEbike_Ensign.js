// =========================== Required and fixed Input ===========================
const fs = require('fs');
var _ = require('lodash');
var request = require('request');
var isodate = require("isodate");
// const SENSOR_API_BASE_URL = 'http://localhost:8080/SensorThingsService/v1.0';
// const SENSOR_API_BASE_URL = 'http://gisstudio.hft-stuttgart.de/FROST-icity/v1.0';
const SENSOR_API_BASE_URL = 'http://193.196.138.56:8080/frost-hft-icityebike/v1.0';

const SENSOR_API_FINAL_URL = '/Observations';
// =====================Set File Location==============
var fileN = 'Data/ebikeDataFTP/Data_FTP_Log_2018_05/events_2018-05-16.log'; //input log file name

// uncomment to execute if file can be load [vin] of the first file
//console.log('First json vin: ', obj2[1].vin);  
//* variable for STA service
var ebike_vin;              //bike vin
var st_id;                  //IoT id of the Sensors and Things
var dataStr_id;             //IoT id of the observed properties
var execute = true;         //Default at false to show the result in terminal first (not POST yet)

var DS_ID_List = {
    "fuelLevel": 1,
    "batteryVoltageBike": 2,
    "mileage": 3,
    "pedalForce": 4,
    "speed": 5,
    "light": 6,
    "charging": 7,
    "motorSupportMin": 8,
    "motorSupportMax": 9,
    "motorSupportavg": 10,
    "motorSupportfirstVal": 11,
    "motorSupportfirstValTime": 12,
    "motorSupportlastVal": 13,
    "motorSupportcount": 14,
    "motorSupporttimeSpan": 15,
    "uuid": 16,
    "accuracy": 17,
    "latitude": 18,
    "longitude": 19,
    "altitude": 20,
}

//* Function "generateRequestfromJSON" make a POST request to the STA
//* Depended on the incoming log file
function generateRequestfromJSON(incomingLog, num, filename) {
    ebike_vin = incomingLog.vin;
    st_id = executeSTid(ebike_vin);
    // need to check light and charging variable here because it is a boolean. 
    var checkLight = _.has(incomingLog, 'status.light');
    var checkCharging = _.has(incomingLog, 'status.charging');
    // need to check altitude variable here because it contain 4th level nested-JSON in JS.
    var checkAltitude = _.has(incomingLog, 'status.geo.altitude');
    //* execute here if this JSON is the ebike data from one of the bike in a HFT area.
    if (st_id != 0) {
        //console.log('"VIN" : ' + ebike_vin);
        //console.log('"IoT id" : ' + st_id);
        //Convert Data Time to TM_Object (ISO 8601 Time string)
        //"2017-11-21_02:01:17.379" ==> 2017-11-21T02:01:17.379+01:00
        //https://www.w3.org/TR/NOTE-datetime
        var dataTimeincoming = _.replace(incomingLog.timestamp, '_', 'T') + 'Z';
        var dataTimeAdjust = isodate(dataTimeincoming);

        //Minus 1 hr offset as the incomingTimestamp has the timezone of UTC+0200
        var dataTimeAdjust2 = new Date(dataTimeAdjust).getTime() - (1 * 60 * 60 * 1000);
        var dataTime = new Date(dataTimeAdjust2).toISOString();
        // condition if(...) will check if the incoming JSON has the field of data or not
        // variable id matches the datastream ID in STA
        // variable dataStr_[Entity's name] constructs the POST request Which will sent through function postSTA(...) 
        if (incomingLog.status.fuelLevel) {
            // var id = st_id;
            var id = 1 + (20 * (st_id - 1));
            var dataStr_fuelLevel = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.status.fuelLevel,
                "Datastream": { "@iot.id": id }
            };
            if (execute) {
                postSTA(dataStr_fuelLevel, num, filename);
            } else {
                console.log(`DataStream Body fuelLevel: ` + JSON.stringify(dataStr_fuelLevel));
                console.log('---------------------------------------------------')
            };
        }
        if (incomingLog.status.batteryVoltageBike) {
            // var id = st_id + (4 * 1);
            var id = 2 + (20 * (st_id - 1));
            var dataStr_batteryVoltageBike = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.status.batteryVoltageBike,
                "Datastream": { "@iot.id": id }
            };
            if (execute) {
                postSTA(dataStr_batteryVoltageBike, num, filename);
            } else {
                console.log(`DataStream Body batteryVoltageBike: ` + JSON.stringify(dataStr_batteryVoltageBike));
                console.log('---------------------------------------------------')
            };
        }
        if (incomingLog.status.mileage) {
            var id = 3 + (20 * (st_id - 1));
            var dataStr_mileage = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.status.mileage,
                "Datastream": { "@iot.id": id }
            };
            if (execute) {
                postSTA(dataStr_mileage, num, filename);
            } else {
                console.log(`DataStream Body mileage: ` + JSON.stringify(dataStr_mileage));
                console.log('---------------------------------------------------')
            };
        }
        if (incomingLog.change.pedalForce) {
            var id = 4 + (20 * (st_id - 1));
            var dataStr_pedalForce = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.change.pedalForce,
                "Datastream": { "@iot.id": id }
            };
            if (execute) {
                postSTA(dataStr_pedalForce, num, filename);
            } else {
                console.log(`DataStream Body pedalForce: ` + JSON.stringify(dataStr_pedalForce));
                console.log('---------------------------------------------------')
            };
        }
        if (incomingLog.change.speed) {
            var id = 5 + (20 * (st_id - 1));
            var dataStr_speed = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.change.speed,
                "Datastream": { "@iot.id": id }
            };
            if (execute) {
                postSTA(dataStr_speed, num, filename);
            } else {
                console.log(`DataStream Body speed: ` + JSON.stringify(dataStr_speed));
                console.log('---------------------------------------------------')
            };
        }
        if (checkLight) {
            var id = 6 + (20 * (st_id - 1));
            var dataStr_light = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.status.light,
                "Datastream": { "@iot.id": id }
            };
            if (execute) {
                postSTA(dataStr_light, num, filename);
            } else {
                console.log(`DataStream Body light: ` + JSON.stringify(dataStr_light));
                console.log('---------------------------------------------------')
            };
        }
        if (checkCharging) {
            var id = 7 + (20 * (st_id - 1));
            var dataStr_charging = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.status.charging,
                "Datastream": { "@iot.id": id }
            };
            if (execute) {
                postSTA(dataStr_charging, num, filename);
            } else {
                console.log(`DataStream Body charging: ` + JSON.stringify(dataStr_charging));
                console.log('---------------------------------------------------')
            };
        }
        if (incomingLog.change.motorSupport) {
            var id = 8 + (20 * (st_id - 1));
            var dataStr_motorSupportMin = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.change.motorSupport.min,
                "Datastream": { "@iot.id": id }
            };
            if (execute) {
                postSTA(dataStr_motorSupportMin, num, filename);
            } else {
                console.log(`DataStream Body motorSupportMin: ` + JSON.stringify(dataStr_motorSupportMin));
            };
        }
        if (incomingLog.change.motorSupport) {
            var id = 9 + (20 * (st_id - 1));
            var dataStr_motorSupportMax = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.change.motorSupport.max,
                "Datastream": { "@iot.id": id }
            };
            if (execute) {
                postSTA(dataStr_motorSupportMax, num, filename);
            } else {
                console.log(`DataStream Body motorSupportMax: ` + JSON.stringify(dataStr_motorSupportMax));
            };
        }
        if (incomingLog.change.motorSupport) {
            var id = 10 + (20 * (st_id - 1));
            var dataStr_motorSupportavg = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.change.motorSupport.avg,
                "Datastream": { "@iot.id": id }
            };
            if (execute) {
                postSTA(dataStr_motorSupportavg, num, filename);
            } else {
                console.log(`DataStream Body motorSupportavg: ` + JSON.stringify(dataStr_motorSupportavg));
            };
        }
        if (incomingLog.change.motorSupport) {
            var id = 11 + (20 * (st_id - 1));
            var dataStr_motorSupportfirstVal = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.change.motorSupport.firstVal,
                "Datastream": { "@iot.id": id }
            };
            if (execute) {
                postSTA(dataStr_motorSupportfirstVal, num, filename);
            } else {
                console.log(`DataStream Body motorSupportfirstVal: ` + JSON.stringify(dataStr_motorSupportfirstVal));
            };
        }
        if (incomingLog.change.motorSupport) {
            var id = 12 + (20 * (st_id - 1));
            var dataStr_motorSupportfirstValTime = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.change.motorSupport.firstValTime,
                "Datastream": { "@iot.id": id }
            };
            if (execute) {
                postSTA(dataStr_motorSupportfirstValTime, num, filename);
            } else {
                console.log(`DataStream Body motorSupportfirstValTime: ` + JSON.stringify(dataStr_motorSupportfirstValTime));
            };
        }
        if (incomingLog.change.motorSupport) {
            var id = 13 + (20 * (st_id - 1));
            var dataStr_motorSupportlastVal = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.change.motorSupport.lastVal,
                "Datastream": { "@iot.id": id }
            };
            if (execute) {
                postSTA(dataStr_motorSupportlastVal, num, filename);
            } else {
                console.log(`DataStream Body motorSupportlastVal: ` + JSON.stringify(dataStr_motorSupportlastVal));
            };
        }
        if (incomingLog.change.motorSupport) {
            var id = 14 + (20 * (st_id - 1));
            var dataStr_motorSupportcount = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.change.motorSupport.count,
                "Datastream": { "@iot.id": id }
            };
            if (execute) {
                postSTA(dataStr_motorSupportcount, num, filename);
            } else {
                console.log(`DataStream Body motorSupportcount: ` + JSON.stringify(dataStr_motorSupportcount));
            };
        }
        if (incomingLog.change.motorSupport) {
            var id = 15 + (20 * (st_id - 1));
            var dataStr_motorSupporttimeSpan = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.change.motorSupport.timeSpan,
                "Datastream": { "@iot.id": id }
            };
            if (execute) {
                postSTA(dataStr_motorSupporttimeSpan, num, filename);
            } else {
                console.log(`DataStream Body motorSupporttimeSpan: ` + JSON.stringify(dataStr_motorSupporttimeSpan));
            };
        }
        if (incomingLog.uuid) {
            var id = 16 + (20 * (st_id - 1));
            var dataStr_uuid = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.uuid,
                "Datastream": { "@iot.id": id }
            };
            if (execute) {
                postSTA(dataStr_uuid, num, filename);
            } else {
                console.log(`DataStream Body uuid: ` + JSON.stringify(dataStr_uuid));
                console.log('---------------------------------------------------')
            };
        }
        if (incomingLog.status.geo) {
            var acc_id = 17 + (20 * (st_id - 1));
            var lat_id = 18 + (20 * (st_id - 1));
            var lon_id = 19 + (20 * (st_id - 1));
            var dataStr_acc = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.status.geo.accuracy,
                "Datastream": { "@iot.id": acc_id }
            };
            var dataStr_lat = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.status.geo.latitude,
                "Datastream": { "@iot.id": lat_id }
            };
            var dataStr_lon = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.status.geo.longitude,
                "Datastream": { "@iot.id": lon_id }
            };
            if (execute) {
                postSTA(dataStr_acc, num, filename);
                postSTA(dataStr_lat, num, filename);
                postSTA(dataStr_lon, num, filename);
            } else {
                console.log(`DataStream Body accuracy: ` + JSON.stringify(dataStr_acc));
                console.log(`DataStream Body latitude: ` + JSON.stringify(dataStr_lat));
                console.log(`DataStream Body longitude: ` + JSON.stringify(dataStr_lon));
            };
        }
        if (checkAltitude) {
            id = 20 + (20 * (st_id - 1));
            var dataStr_altitude = {
                "phenomenonTime": dataTime,
                "resultTime": dataTime,
                "result": incomingLog.status.geo.altitude,
                "Datastream": { "@iot.id": id }
            };
            if (execute) {
                postSTA(dataStr_altitude, num, filename);
            } else {
                console.log(`DataStream Body altitude: ` + JSON.stringify(dataStr_altitude));
                console.log('---------------------------------------------------')
            };
        }
        //Build the Observation Body
        //PhenomenonTime is the time instant or period of when the Observation happens

        // check some variable here
        //console.log('...' + checkAltitude);

    } else {
        /////////////////////////////////////////////
        // In case that the ebike are not from HFT//
        //////////////////////////////////////////// 
        //console.log('==============ebike not from HFT!==================');
    }
    //console.log('================================');
}

// * Function to check the ebikeVIN id here
function executeSTid(VIN) {
    if (VIN == "eBike20131126003c") { var id = 1; }
    else if (VIN == "eBike201209280029") { var id = 2; }
    else if (VIN == "eBike20131107003d") { var id = 3; }
    else if (VIN == "eBike201311270017") { var id = 4; }
    else if (VIN == "eBike20131127000a") { var id = 5; }
    else if (VIN == "eBike20131127000f") { var id = 6; }
    else { var id = 0; }
    return id;
}

// * Function to make POST request to the STA
function postSTA(item, i, filename) {
    let headers = { 'Content-Type': 'application/json' };
    let options = {
        url: SENSOR_API_BASE_URL + SENSOR_API_FINAL_URL,
        headers: headers,
        method: 'POST',
        body: JSON.stringify(item),
    }
    //console.log(item);
    request(options, function (error, httpResponse, body) {
        if (error) {
            return console.error(`Post data failed:`, error);
        }
        // Print out the response body
        console.log(`Post datastream [${i}] from [${filename}] to STA successfully!!`, body);
    })
}

var start_index = 0; // Number of file that start the process!
var input_folder = 'Data/ebikeDataFTP/Data_FTP_Log_2019_05_2/';
function init() {
    //=========================== Loading File ===========================
    fs.readdir(input_folder, function (err, files) {
        // console.log(err)
        processingFile(files, start_index)
        //* Load the .log file and turn into a JSON format

    });

}
function processingFile (files, index) {
    if (typeof files[index] == "undefined") {
        console.log('=====  Processing all files done!! =====');
    } else {
        console.log('=====  Starting POST FTP to STA OBS from LOG file:' + files[index] + '  =====');
    var fileInput = input_folder + files[index];
    var obj = (fs.readFileSync(fileInput, 'utf8'));
    obja = obj.replace(/\n.*$/, '')
    obj1 = obja.replace(/\n/g, ',');
    obj11 = '[' + obj1 + ']';
    var obj2 = JSON.parse(obj11);
    var startLine = 0;          // Set the starting line to process, 0 for processing all line!
    var maxLine = obj2.length;
    var endLine = maxLine;      // Set to maxLine in case of processing all line!
    console.log('file %s load successfully', fileN);
    console.log(`--processing file: ${files[index]} --`)
    for (let i = startLine; i < endLine /*obj2.length*/; i++) {
        setTimeout(function cb() {
            console.log('Read Object : ' + i)
            generateRequestfromJSON(obj2[i], i,files[index]);
            if (i == (endLine -1) && index<files.length) {
                processingFile(files, index+1)
            }
        }, 30 * (i - startLine));
        
    } // Set the time out dealy of 100 ms
}
    }
    

init();