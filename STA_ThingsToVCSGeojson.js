
// Read Synchrously
var fs = require("fs");
console.log("\n *START* \n");
var content = fs.readFileSync("./Data/STA_3DIoT_GISFCU/Things.json");
var Things = JSON.parse(content);
var ThingsValue = Things.value
var GeoFeature = []
// console.log(ThingsValue)
var i = 0
for(var value in ThingsValue){
    console.log(`looping through STA Things: ${i +1}`);
    GeoFeature.push({
        "name":`${ThingsValue[i].properties.englishName}`,
        "STA":ThingsValue[i]["Locations@odata.navigationLink"],
        "state": "dynamic",
        "layerId": "geojson_Things_GISFCU",
        "featureType": "simple",
        "type": "Feature",
        "geometry": {
            "type": "Point",
            "coordinates": [
                ThingsValue[i].Locations[0].location.coordinates[0],
                ThingsValue[i].Locations[0].location.coordinates[1]
            ]
        },
        "properties" : {
            "id" : "geojson_Things_GISFCU",
            "Name" : `${ThingsValue[i].properties.englishName}`,
            "type": `${ThingsValue[i].properties.description}`,
            "STA":ThingsValue[i]["Locations@odata.navigationLink"],

        },
        "vcsMeta": {
            "style": {
                "type": "vector",
                "text": {
                    "font": "bold 18px sans-serif",
                    "fill": {
                        "color": [
                            51,
                            51,
                            51,
                            1
                        ]
                    },
                    "textBaseline": "bottom",
                    "offsetY": -15,
                    "offsetX": 0
                },
                "label": ThingsValue[i].properties.englishName,
                "image": {
                    "scale": 1,
                    "fill": {
                        "color": [
                            0,64,255,
                            0.4
                        ]
                    },
                    "radius": 5,
                    "stroke": {
                        "color": [
                            0,
                            0,
                            0,
                            1
                        ],
                        "width": 1,
                        "lineDash": null
                    }
                }
            }
        },
        "id": ThingsValue[i].properties.englishName + "GeoJSONVSC"
    })
    i += 1
}
console.log(`--- Looping throuhg all value in Things ==> Writing to File ...`);

var geojsonBody = {
    "id": "AwDeg9gJfjnaQqbhX",
    "type": "FeatureCollection",
    "features": GeoFeature,
    "featureType": "simple",
    "vcsMeta": {
        "version": "1.0",
        "altitudeMode": "clampToGround",
        "style": {
            "type": "vector",
            "fill": {
                "color": [
                    255,
                    255,
                    128,
                    0.4
                ]
            },
            "stroke": {
                "color": [
                    51,
                    153,
                    204,
                    1
                ],
                "width": 1.25,
                "lineDash": null
            },
            "text": {
                "font": "bold 18px Times New Roman\", Times, serif",
                "fill": {
                    "color": [
                        255,
                        0,
                        0,
                        1
                    ]
                },
                "stroke": {
                    "color": [
                        0,
                        0,
                        0,
                        1
                    ],
                    "width": 1,
                    "lineDash": null
                },
                "textBaseline": "bottom",
                "offsetY": -15,
                "offsetX": 0
            }
        }
    }
}
let data = JSON.stringify(geojsonBody);
fs.writeFileSync('./Data/STA_3DIoT_GISFCU/GeoJSONforVCS.json', data);
console.log(`--- Program done ...`);
