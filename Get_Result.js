const fs = require('fs');
var _ = require('lodash');
var request = require('request');
var jsonfile = require('jsonfile');
//=======================================================================
const SENSOR_API_BASE_URL = 'http://localhost:8080/SensorThingsService/v1.0';
const SENSOR_API_FINAL_URL = '/Datastreams(72)/Observations?$select=result,resultTime&$orderby=resultTime&$top=20'
//Datastream 72 ==> Latitude of ebike 4
//Datastream 76 ==> Longitude of ebike 4
//Datastream 80 ==> Altitude of ebike 4
//=======================================================================
var ResponseBody, LatitudeBody, LongitudeBody, AltitudeBody;
function getSTA(x, cb) {
    let headers = { 'Content-Type': 'application/json' };
    let options = {
        url: `http://localhost:8080/SensorThingsService/v1.0/Datastreams(${x})/Observations?$select=result,resultTime&$orderby=resultTime&$top=20&$filter=hour(phenomenonTime) ge 17 and hour(phenomenonTime) le 18`,
        headers: headers,
        method: 'GET',
        //body: JSON.stringify(item),
        //url: 'http://localhost:8080/SensorThingsService/v1.0/Datastreams('+x+')/Observations?$select=result,resultTime&$orderby=resultTime&$top=20',
    }
    //console.log(item);
    request(options, function (error, httpResponse, body) {
        if (error) {
            return console.error(`Get data failed:`, error);
        }
        ResponseBody = JSON.parse(body);
        var checkNext = _.has(ResponseBody, '@iot.nextLink');
        console.log('Check next link : ',checkNext);
        // Print out the response body
        //console.log(body);
    })
}
getSTA(72);
setTimeout(function cb() {
    LatitudeBody = ResponseBody;
    //console.log('result1=LAT========>' + LatitudeBody['value'][0]['result']);
    //console.log('result1=Time=======>' + LatitudeBody['value'][0]['resultTime']);
    console.log(LatitudeBody);
    setTimeout(function cb() {
        getSTA(76);
        setTimeout(function cb() {
            LongitudeBody = ResponseBody;
            //console.log('result2=LON========>' + LongitudeBody['value'][0]['result']);
            //console.log('result2=Time=======>' + LongitudeBody['value'][0]['resultTime']);
            //console.log(LongitudeBody);
        }, 1000);
    }, 1000);

}, 1000);