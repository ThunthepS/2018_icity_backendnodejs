// =========================== Required and fixed Input ===========================
const fs = require('fs');
var _ = require('lodash');
var isodate = require("isodate");
var jsonfile = require('jsonfile');
// =====================Set File Location==============
var fileN = 'Data/ebikeDataFTP/Data_FTP_Log_2017_12/events_2017-12-04.log'; //input log file name
//=========================== Loading File ===========================
//* Load the .log file and turn into a JSON format
var obj = (fs.readFileSync(fileN, 'utf8'));
obja = obj.replace(/\n.*$/, '')
obj1 = obja.replace(/\n/g, ',');
obj11 = '[' + obj1 + ']';
var obj2 = JSON.parse(obj11);
console.log('file %s load successfully', fileN);
// ===============Setting Up Everything ==================================
// ======================================================================
var ebike_vin; //bike vin
var st_id; //IoT id of the Sensors and Things
var dataStr_id; //IoT id of the observed properties
var startLine = 0; // Set the starting line to process, 0 for processing all line!
var maxLine = obj2.length;
var endLine = maxLine; // Set to maxLine in case of processing all line!
var execute = true; //Default at false to show the result in terminal first (not POST yet)
var LocationData = [];
LocationData[0] = [];
var delayFactor = 1;
function init() {
    for (let i = startLine; i < endLine /*obj2.length*/ ; i++) {
        setTimeout(function cb() {
            console.log('Read Object : ' + i)
            generateRequestfromJSON(obj2[i], i);
        }, delayFactor * (i - startLine));
    } 
    setTimeout(function cb() {
        console.log("--All objects are read--")
        var result = LocationData;
        jsonfile.writeFile("Ebike-1-newMethod.json", result, function (err) {
            if (err) throw err;
            console.log('--filesaved--');
        })
    }, delayFactor * (endLine)+delayFactor);
} // ===========Main function for building CZML ==========================
function generateRequestfromJSON(incomingLog, num) {
    ebike_vin = incomingLog.vin;
    st_id = executeSTid(ebike_vin);
    if (st_id != 0) {
        var dataTimeincoming = _.replace(incomingLog.timestamp, '_', 'T') + 'Z';
        var dataTimeAdjust = isodate(dataTimeincoming);
        var dataTimeAdjust2 = new Date(dataTimeAdjust).getTime() - (1 * 60 * 60 * 1000);
        var dataTime = new Date(dataTimeAdjust2).toISOString();
        if (st_id == 2 || st_id == 3 || st_id == 4) {
            if (incomingLog.change.geo) {
                LocationData[0].push(dataTime); //lon for locate
                LocationData[0].push(incomingLog.change.geo.longitude); //lon for locate
                LocationData[0].push(incomingLog.change.geo.latitude); //lat for locate
                LocationData[0].push(incomingLog.change.geo.altitude); //alt for locate
            }
        }
    }
}

// * Function to check the ebikeVIN id here
function executeSTid(VIN) {
    if (VIN == "eBike20131126003c") {
        var id = 1;
    } else if (VIN == "eBike201209280029") {
        var id = 2;
    } else if (VIN == "eBike20131107003d") {
        var id = 3;
    } else if (VIN == "eBike201311270017") {
        var id = 4;
    } else {
        var id = 0;
    }
    return id;
}



init();