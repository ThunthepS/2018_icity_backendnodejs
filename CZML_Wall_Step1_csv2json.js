//To change before running!!
////////////////////////////////////////////////////// 
////////////////// Setting Here ////////////////// 
///////////////////////////////////////////////////
var file_in_gpx = './Data/ExportToCZML/11_22_2017_GPX_1_Fix.csv';
var file_outname = './Data/ExportToCZMLOut/11_22_2017_GPX_1_location.json'; //Altitude is Heartrate!!
var StartLine = 1;
var jsonfile = require('jsonfile');
var parse = require('csv-parse');
var fs = require('fs');
var LocationData, out,endstring;
LocationData = [];
var codeString = "";
fs.readFile(file_in_gpx, function (err, output) {
    if (err) throw err;
    out = output;
    parse(out, {
        comment: '#'
    }, function (err, output) {
        for (let i = StartLine; i < output.length /*size of the csv -1*/ ; i++) {
            if (i == output.length -1) {
                endstring = ')'
            } else {
                endstring = '),'
            }
            codeString += 'Cesium.Cartographic.fromDegrees('+ 
                        parseFloat(output[i][1]) +
                        ',' +
                        parseFloat(output[i][0]) + 
                        endstring
            //LocationData.push(str);
        }
        jsonfile.writeFile(file_outname, codeString, function (err) {
            if (err) throw err;
            console.log('filesaved');
        })
    });
});