    // =========================== Require setup ===========================
    var request = require('request');
    // =========================== Input setup ===========================
    const SENSOR_API_BASE_URL = 'http://covidsta.hft-stuttgart.de/server/v1.1';
    const SENSOR_API_FINAL_URL = '/Observations';
    const query = "?$filter=phenomenonTime%20ge%202020-05-06T00:00:00.000Z"
    var start = 1; //First IoT to delete
    var end = 1;//Last IoT to delete
    // ==============================deleteAll not used anymore!!=====================================
    function deleteSTA(i) {
        let headers = {'Content-Type': 'application/json'};
        let options = {
            url: SENSOR_API_BASE_URL + SENSOR_API_FINAL_URL + query,
            headers: headers,
            method: 'DELETE',
            }
        request(options, function (error, httpResponse, body) {
            if (error || i == (end)) {
                return console.log('Delete datastream number: '+ i+ ' >> All Job Complete');
              } else { 
            // Print out the response body
                console.log(('Delete datastream number: '+ i + ' on STA=> successfully =>> Response Body :'), body);
                deleteSTA(i+1);
            }
        })
    }
deleteSTA(start);
//deleteAll();

