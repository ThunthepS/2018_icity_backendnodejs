const fs = require('fs');
var json2csv = require('json2csv');
// ==============Loading Properties==============
var file = 'events_2019-09-04'; //input log file name
//===============================================
// Loading File
var fileN = file + '.log';
var outname = `${file}_out.csv`;
var obj = (fs.readFileSync(fileN, 'utf8'));
var obja = obj.replace(/\n.*$/, '')
var objb = obja.replace(/\n/g, ',')
var objc = '[' + objb + ']';
var obj2 = JSON.parse(objc);
console.log('file %s load successful', fileN);
function conjson() {
var fields = [  'vin', 
				'change.geo.altitude', 
				'change.geo.latitude',
				'change.geo.longitude',
				'change.geo.accuracy',
				'change.batteryVoltageBike',
				'change.connection.connected',
				'change.mileage',
				'change.pedalForce',
				'change.speed',
				'change.motorSupport.min',
				'change.motorSupport.avg',
				'change.motorSupport.max',
				'change.motorSupport.count',
				'change.motorSupport.firstValTime',
				'change.motorSupport.timeSpan',
				'change.motorSupport.lastVal',
				'change.light',
				'change.fuelLevel',
				'vin','uuid',
				'status.allDoorsClosed', 
				'status.fuelLevel',
				'status.charging',
				'status.ignitionOn',
				'status.geo.altitude',
				'status.geo.latitude',
				'status.geo.accuracy',
				'status.geo.longitude',
				'status.powerState',
				'status.light',
				'status.connection.connected',
				'status.connection.since',
				'status.locked',
				'status.batteryVoltageBike',
				'status.mileage',
				'status.batteryLevel',
				'timestamp'
				];
var csv = json2csv({ data: obj2, fields: fields });
fs.writeFile(outname, csv, function(err) {
  if (err) throw err;
  console.log('file %s csv saved', outname);
});
}
try {
	conjson()
}
catch(err) {
	console.log('->  Update of json successful!\n' + err);
}
