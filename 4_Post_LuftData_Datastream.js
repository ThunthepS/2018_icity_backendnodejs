    // =========================== Required and fixed Input ===========================
    var jsonfile = require('jsonfile');
    var request = require('request');

    // =========================== Input setup ===========================
    const SENSOR_API_FINAL_URL = '/Datastreams';
// const SENSOR_API_BASE_URL = 'http://localhost:8080/SensorThingsService/v1.0';
// const SENSOR_API_BASE_URL = 'http://193.196.138.56:8080/frost-hft-sensors/v1.0';
// const SENSOR_API_BASE_URL = 'http://193.196.138.56:8080/frost-citythings-musi/v1.0';
// const SENSOR_API_BASE_URL = 'http://193.196.138.56/musithings/v1.0';
const SENSOR_API_BASE_URL = 'http://193.196.138.56/frost-luftdata-api/v1.1';

// const SENSOR_API_BASE_URL = 'http://193.196.138.56:8080/frost-hft-iplon/v1.0';
//const SENSOR_API_BASE_URL = 'http://gisstudio.hft-stuttgart.de/FROST-icity/v1.0';   

    // Set variable depend on the object in SensorThings Service
    var IdObsProp_First = 3; //
    var IdObsProp_Last = 5; //
    var IdThings_First = 93; //
    var IdThings_Last = 102; //
    // ============================ Define Variable =========================
    var datastreamBody = [];
    var DS_Name;
    var DS_Description;
    var DS_obType;
    var UoM_Name;
    var UoM_Symbol;
    var UoM_Definition;
    var s_id;
    var CoreDS_Name;
    var count = 0;

    function Post_Sensorthings() {
        //loop through all STA services
        //* o_id --> ObservedProperties
        //* st_id --> Things and Sensors ID
        for (let st_id = IdThings_First; st_id <= IdThings_Last; st_id++) { /*Whole range 1 to 4*/
            for (let o_id = IdObsProp_First; o_id <= IdObsProp_Last; o_id++) { /*Whole range 1 to 16*/

                // match the IoT id of <Things and Sensors>
                if (o_id == 3) {
                    CoreDS_Name = "noise_LAeq"
                    DS_Name = `${CoreDS_Name}`,
                        DS_Description = `Datastream for recording ${CoreDS_Name} (see https://bit.ly/2KcWV0E for more detailed relations)`,
                        DS_obType = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement",
                        UoM_Name = "dB(A)",
                        UoM_Definition = "https://en.wikipedia.org/wiki/Decibel",
                        UoM_Symbol = "dB(A)"
                }
                if (o_id == 4) {
                    CoreDS_Name = "noise_LA_min"
                    DS_Name = `${CoreDS_Name}`,
                        DS_Description = `Datastream for recording ${CoreDS_Name} (see https://bit.ly/2KcWV0E for more detailed relations)`,
                        DS_obType = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement",
                        UoM_Name = "dB(A)",
                        UoM_Definition = "https://en.wikipedia.org/wiki/Decibel",
                        UoM_Symbol = "dB(A)"
                }
                if (o_id == 5) {
                    CoreDS_Name = "noise_LA_max"
                    DS_Name = `${CoreDS_Name}`,
                        DS_Description = `Datastream for recording ${CoreDS_Name} (see https://bit.ly/2KcWV0E for more detailed relations)`,
                        DS_obType = "http://www.opengis.net/def/observationType/OGC-OM/2.0/OM_Measurement",
                        UoM_Name = "dB(A)",
                        UoM_Definition = "https://en.wikipedia.org/wiki/Decibel",
                        UoM_Symbol = "dB(A)"
                }
                //* Format the datastream JSON body
                datastreamBody[count] = {
                    "name": DS_Name,
                    "description": DS_Description,
                    "observationType": DS_obType,
                    "unitOfMeasurement": {
                        "name": UoM_Name,
                        "symbol": UoM_Symbol,
                        "definition": UoM_Definition
                    },
                    // "Thing":{"@iot.id":st_id},
                    "Thing": {
                        "@iot.id": st_id
                    },
                    "ObservedProperty": {
                        "@iot.id": o_id
                    },
                    "Sensor": {
                        "@iot.id": 2
                    }
                };
                //console.log(JSON.stringify(datastreamBody));  */ command to check the datastream body
                //console.log(`Sending POST request of DataStream [ObsPropID: ${o_id}][ThingID:${st_id}][SensorID:${s_id}] `)
                count++;
            }
        }
        postSTA(datastreamBody, 0);
    }

    function postSTA(DS_Body, i) {
        //var x = 81 + i;
        let headers = {
            'Content-Type': 'application/json'
        };
        let options = {
            url: SENSOR_API_BASE_URL + SENSOR_API_FINAL_URL,
            headers: headers,
            method: 'POST',
            body: JSON.stringify(DS_Body[i]),
        }
        //console.log(item);

        request(options, function (error, httpResponse, body) {
            if (error || i == (DS_Body.length - 1)) {
                console.log(`Sending POST request of DataStream [${i}] successfully`);
                console.log(`>> All Job Complete`);
                return console.error(`show error >>` + error);
            }
            // If not error
            //console.log("-------------Post datastream to STA successful!-------------", body);
            console.log(`Sending POST request of DataStream [${i}] => successfully`, body);
            //console.log(DS_Body[i]);
            postSTA(DS_Body, i + 1)

        })
    }
    Post_Sensorthings();