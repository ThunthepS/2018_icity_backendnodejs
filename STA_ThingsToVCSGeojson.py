import time
import pandas as pd
from pandas.io.json import json_normalize 
docName = "joePythongSTA"
start_time = time.time() 

URL = "./Data/STA_3DIoT_GISFCU/Things.json"


df = json_normalize(URL, 'locations', ['date', 'number', 'name'],record_prefix='locations_')

print(f"{docName} done: using {round((time.time() - start_time),2)} second")