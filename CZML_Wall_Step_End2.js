//To change before running!!
////////////////////////////////////////////////////// 
////////////////// Setting Here ////////////////// 
///////////////////////////////////////////////////
var file_in_gpx = './Data/ExportToCZML/11_22_2017_GPX_1_Fix.csv';
var file_in_tcx = './Data/ExportToCZML/11_22_2017_TCX_1_Fix.csv';
var file_in_height = './Data/ExportToCZML/11_22_2017_GPX_1_Fix_height.json';

var file_outname_base = "czml_11_22_2017_1";
var file_outname_ExtTemp = "./Data/ExportToCZMLOut/" + file_outname_base + "_Temp_Wall.json";
var file_outname_ExtHR = "./Data/ExportToCZMLOut/" + file_outname_base + "_HR_Wall.json";
var file_outname_ExtFuel = "./Data/ExportToCZMLOut/" + file_outname_base + "_Fuel_Wall.json";
var file_outname_ExtSpeed = "./Data/ExportToCZMLOut/" + file_outname_base + "_Speed_Wall.json";

var idSuffix = "Garmin_11_22_2017_1"; // ex "Garmin_11_22_2017_2"
var textLabel = "HeartRate Level\nEbike-ID: 02\nParticipant-ID: 02";

////Imported////
//"Garmin_11_22_2017_1"
//Ebike-ID: 02
//Part-ID: 02

////Imported////
//"Garmin_11_22_2017_2"
//Ebike-ID: 04
//Part-ID: 03
///////////////////////////////////////////////////
///////////////////////////////////////////////////
var StartLine = 1;
var czml_path_width = 4;
var jsonfile = require('jsonfile');
var parse = require('csv-parse');
var fs = require('fs');
var LocationData, out, out2, LdJSON, opLength, startTime, endTime, DistanceM, Speed;
var LocationDataExtrudeSpeed,
    LocationDataExtrudeTemperature,
    LocationDataExtrudeHR,
    LocationDataExtrudeDM,
    LocationDataExtrudeFeul;
//const LastLine = 200;
let height_data = fs.readFileSync(file_in_height);  
let height_json = JSON.parse(height_data);  
fs.readFile(file_in_gpx, function (err, output) {
    if (err) throw err;
    out = output;
    parse(out, {
        comment: '#'
    }, function (err, output) {
        //console.log(output[1][1]);

        var tempData = [output[StartLine][3]];
        var HrData = [output[StartLine][3]];
        var RlAlt = [output[StartLine][3]];
        tempData.push(parseFloat(output[StartLine][4]));
        HrData.push(parseFloat(output[StartLine][5]));
        RlAlt.push(parseFloat(output[StartLine][2]));
        // --------------------------------------------------------
        LocationData = [];
        LocationDataExtrudeHR = [];
        LocationDataExtrudeSpeed = [];
        LocationDataExtrudeTemperature = [];
        LocationDataExtrudeFeul = [];
        // --------------------------------------------------------
        LocationData.push(parseFloat(output[StartLine][1]));
        LocationData.push(parseFloat(output[StartLine][0]));
        LocationData.push(parseFloat(output[StartLine][2]));
        // -------------------Heart rate Extrude-------------------
        LocationDataExtrudeHR.push(parseFloat(output[StartLine][1]));
        LocationDataExtrudeHR.push(parseFloat(output[StartLine][0]));
        LocationDataExtrudeHR.push(height_json[0] + (parseFloat(output[StartLine][5]) - 70));
        // -------------------Temperatue Extrude------------------- 
        // Sample Equation Y = (10 * X) - 20
        LocationDataExtrudeTemperature.push(parseFloat(output[StartLine][1]));
        LocationDataExtrudeTemperature.push(parseFloat(output[StartLine][0]));
        LocationDataExtrudeTemperature.push(height_json[0] + (parseFloat(output[StartLine][4]) - 21));
        // -------------------Temperatue Fuel------------------- 
        // Sample Equation Y = (10 * X) - 20
        LocationDataExtrudeFeul.push(parseFloat(output[StartLine][1]));
        LocationDataExtrudeFeul.push(parseFloat(output[StartLine][0]));
        LocationDataExtrudeFeul.push(height_json[0] + (parseFloat(output[StartLine][7])));
        // ==============================================
        locationDate = new Date(output[StartLine][3]);

        Tim = locationDate.getTime();
        opLength = output.length - 1;
        startTime = output[StartLine][3];
        endTime = output[opLength - 1][3];
        for (let i = StartLine + 1; i < output.length /*size of the csv -1*/ ; i++) {
            //var locationDate = new Date(output[i][3]); //time
            //var TimSec = locationDate.getTime();
            //LocationData.push((TimSec - Tim) / 1000);
            // -----------------------------------------
            LocationData.push(parseFloat(output[i][1])); //lon for locate
            LocationData.push(parseFloat(output[i][0])); //lat for locate
            LocationData.push(height_json[i]); //alt for locate
            // -------------------Heart rate Extrude-------------------
            LocationDataExtrudeHR.push(parseFloat(output[i][1]));
            LocationDataExtrudeHR.push(parseFloat(output[i][0]));
            LocationDataExtrudeHR.push((height_json[i] +(parseFloat(output[i][5])*5) ));
            // -----------------------------------------
            LocationDataExtrudeTemperature.push(parseFloat(output[i][1]));
            LocationDataExtrudeTemperature.push(parseFloat(output[i][0]));
            LocationDataExtrudeTemperature.push((height_json[i] +(parseFloat(output[i][4])*5) ));
            // -----------------------------------------
            LocationDataExtrudeFeul.push(parseFloat(output[i][1]));
            LocationDataExtrudeFeul.push(parseFloat(output[i][0]));
            LocationDataExtrudeFeul.push((height_json[i] +(parseFloat(output[i][7]))*2 ));


            tempData.push(output[i][3]); //temp property time
            tempData.push(parseFloat(output[i][4])); //temp property for display
            HrData.push(output[i][3]); //HeartRate property time
            HrData.push(parseFloat(output[i][5])); //HeartRate property for display
            RlAlt.push(output[i][3]); //// alt property for time
            RlAlt.push(parseFloat(output[i][2])); //// alt property for display
        }

        //To Fix!!
        fs.readFile(file_in_tcx, function (err, output2) {
            if (err) throw err;
            out2 = output2;
            parse(out2, {
                comment: '#'
            }, function (err, output2) {
                var ze = 0;
                DistanceM = [output2[StartLine][0]];
                Speed = [output2[StartLine][0]];
                DistanceM.push(ze);
                Speed.push(parseFloat(output2[StartLine][2]));
                for (let i = StartLine + 1; i < output2.length /*size of the csv -1*/ ; i++) {
                    var DistanceM_Value = parseFloat([output2[i][1]]) - parseFloat([output2[StartLine][1]]); //time
                    DistanceM.push(output2[i][0]); //DistanceM property time
                    DistanceM.push(DistanceM_Value); //DistanceM for display
                    Speed.push(output2[i][0]); //Speed property time
                    Speed.push(parseFloat(output2[i][2])); //Speed property  for display
                }
                // var result = generateWall(locationData);
                var result_ExtrudeHR = generateWall(LocationDataExtrudeHR, [255, 0, 0, 150],idSuffix);
                var result_ExtrudeTemperature = generateWall(LocationDataExtrudeTemperature, [0, 255, 0, 150],idSuffix);
                var result_ExtrudeFuel = generateWall(LocationDataExtrudeFeul, [0, 0, 225,150],idSuffix);


                // ---------------- The Direct Result --------------------------- 
                // var result = [{
                //     "id": "document",
                //     "name": "Data" + idSuffix,
                //     "version": "1.0"
                // }, {
                //     "id": idSuffix,
                //     "wall": {
                //         "positions": {
                //             "cartographicDegrees": LocationData
                //         },
                //         "material": {
                //             "solidColor": {
                //                 "color": {
                //                     "rgba": [255, 0, 0, 150]
                //                 }
                //             }
                //         }
                //     }
                // }];
                
                // jsonfile.writeFile(file_outname, result, function (err) {
                //     if (err) throw err;
                //     console.log('filesaved');
                // })
                jsonfile.writeFile(file_outname_ExtHR, result_ExtrudeHR, function (err) {
                    if (err) throw err;
                    console.log('filesaved (HR-Extrude)');
                })
                jsonfile.writeFile(file_outname_ExtTemp, result_ExtrudeTemperature, function (err) {
                    if (err) throw err;
                    console.log('filesaved (Temp-Extrude)');
                })
                jsonfile.writeFile(file_outname_ExtFuel, result_ExtrudeFuel, function (err) {
                    if (err) throw err;
                    console.log('filesaved (Fule-Extrude)');
                })
                //}, 1000);
            });
        });

    });
});


var generateWall = function (locationDataFile, rgba_array, suffix) {
    var wall = [{
        "id": "document",
        "name": "Data" + suffix,
        "version": "1.0"
    }, {
        "id": suffix,
        "wall": {
            "positions": {
                "cartographicDegrees": locationDataFile
            },
            "material": {
                "solidColor": {
                    "color": {
                        "rgba": rgba_array
                    }
                }
            }
        }
    }];
    return wall
}