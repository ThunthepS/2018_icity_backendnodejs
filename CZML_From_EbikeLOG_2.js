// =========================== Required and fixed Input ===========================
const fs = require('fs');
var _ = require('lodash');
var isodate = require("isodate");
var jsonfile = require('jsonfile');
// =====================Set File Location==============
var filePath = 'Data/ebikeDataFTP/Data_FTP_Log_2018_05/';
var fileDate = '2018-05-04';
var fileName = 'events_' + fileDate;
var fileType = '.log'
var startTime = fileDate + 'T07:00:00.000Z';
var endTime = fileDate + 'T22:00:00.000Z';
var outPath = 'Data/ExportToCZMLOut_TCU/';
var altitudeOffset = 30;
var fileN = filePath + fileName + fileType;
//var fileN = 'Data/ebikeDataFTP/Data_FTP_Log_2017_12/events_2017-12-04.log'; 
//=========================== Loading File ===========================
//* Load the .log file and turn into a JSON format
var obj = (fs.readFileSync(fileN, 'utf8'));
obja = obj.replace(/\n.*$/, '')
obj1 = obja.replace(/\n/g, ',');
obj11 = '[' + obj1 + ']';
var obj2 = JSON.parse(obj11);
console.log('file %s load successfully', fileN);
// ===============Setting Up Everything ==================================
// ======================================================================
var ebike_vin; //bike vin
var st_id; //IoT id of the Sensors and Things
var dataStr_id; //IoT id of the observed properties
var startLine = 0; // Set the starting line to process, 0 for processing all line!
var maxLine = obj2.length;
var endLine = maxLine; // Set to maxLine in case of processing all line!
var execute = true; //Default at false to show the result in terminal first (not POST yet)
var LocationData1 = [];
var LocationData2 = [];
var LocationData3 = [];
var LocationData4 = [];
var LocationData5 = [];
var LocationData6 = [];
var LocationData7 = [];

var delayFactor = 0.5;
function init() {
    for (let i = startLine; i < endLine /*obj2.length*/ ; i++) {
        setTimeout(function cb() {
            console.log('Read Object : ' + i)
            generateRequestfromJSON(obj2[i], i);
        }, delayFactor * (i - startLine));
    } 
    setTimeout(function cb() {
        console.log("--All objects are read--")
        // var resultTemp = LocationData;
        var result_Ebike1 = CZMLWriter("Ebike-1", LocationData1, [255, 0, 0, 255], [255, 0, 0, 255],[-50, -25]);
        var result_Ebike2 = CZMLWriter("Ebike-2", LocationData2, [0, 255, 0, 255], [0, 255, 0, 255],[-50, -50]);
        var result_Ebike3 = CZMLWriter("Ebike-3", LocationData3, [255, 255, 0, 255], [255, 255, 0, 255],[50, -50]);
        var result_Ebike4 = CZMLWriter("Ebike-4", LocationData4, [155, 0, 0, 255], [155, 0, 0, 255],[50, -25]);
        var result_Ebike6 = CZMLWriter("Ebike-6", LocationData6, [0, 155, 0, 255], [0, 155, 0, 255],[-50, -50]);
        var result_Ebike7 = CZMLWriter("Ebike-7", LocationData7, [0, 155, 155, 255], [0, 155, 155, 255],[50, -50]);


        jsonfile.writeFile(outPath + fileDate + "_Ebike-1"+".json", result_Ebike1, function (err) {
            if (err) throw err;
            console.log('--'+ fileDate + "_Ebike-1" +'filesaved--');
        })
        jsonfile.writeFile(outPath + fileDate + "_Ebike-2"+".json", result_Ebike2, function (err) {
            if (err) throw err;
            console.log('--'+ fileDate + "_Ebike-2" +'filesaved--');
        })
        jsonfile.writeFile(outPath + fileDate + "_Ebike-3"+".json", result_Ebike3, function (err) {
            if (err) throw err;
            console.log('--'+ fileDate + "_Ebike-3" +'filesaved--');
        })
        jsonfile.writeFile(outPath + fileDate + "_Ebike-4"+".json", result_Ebike4, function (err) {
            if (err) throw err;
            console.log('--'+ fileDate + "_Ebike-4" +'filesaved--');
        })
        jsonfile.writeFile(outPath + fileDate + "_Ebike-6"+".json", result_Ebike6, function (err) {
            if (err) throw err;
            console.log('--'+ fileDate + "_Ebike-6" +'filesaved--');
        })
        jsonfile.writeFile(outPath + fileDate + "_Ebike-7"+".json", result_Ebike7, function (err) {
            if (err) throw err;
            console.log('--'+ fileDate + "_Ebike-7" +'filesaved--');
        })
    }, delayFactor * (endLine)+delayFactor);
} // ===========Main function for building CZML ==========================
function generateRequestfromJSON(incomingLog, num) {
    ebike_vin = incomingLog.vin;
    st_id = executeSTid(ebike_vin);
    if (st_id != 0) {
        var dataTimeincoming = _.replace(incomingLog.timestamp, '_', 'T') + 'Z';
        var dataTimeAdjust = isodate(dataTimeincoming);
        var dataTimeAdjust2 = new Date(dataTimeAdjust).getTime() - (1 * 60 * 60 * 1000);
        var dataTime = new Date(dataTimeAdjust2).toISOString();
        if (incomingLog.change.geo) {
            if (st_id == 1) {
                LocationData1.push(dataTime); //lon for locate
                LocationData1.push(incomingLog.change.geo.longitude); //lon for locate
                LocationData1.push(incomingLog.change.geo.latitude); //lat for locate
                LocationData1.push(incomingLog.change.geo.altitude +altitudeOffset); //alt for locate
            }
            if (st_id == 2) {
                LocationData2.push(dataTime); //lon for locate
                LocationData2.push(incomingLog.change.geo.longitude); //lon for locate
                LocationData2.push(incomingLog.change.geo.latitude); //lat for locate
                LocationData2.push(incomingLog.change.geo.altitude +altitudeOffset); //alt for locate
            }
            if (st_id == 3) {
                LocationData3.push(dataTime); //lon for locate
                LocationData3.push(incomingLog.change.geo.longitude); //lon for locate
                LocationData3.push(incomingLog.change.geo.latitude); //lat for locate
                LocationData3.push(incomingLog.change.geo.altitude +altitudeOffset); //alt for locate
            }
            if (st_id == 4) {
                LocationData4.push(dataTime); //lon for locate
                LocationData4.push(incomingLog.change.geo.longitude); //lon for locate
                LocationData4.push(incomingLog.change.geo.latitude); //lat for locate
                LocationData4.push(incomingLog.change.geo.altitude +altitudeOffset); //alt for locate
            }
            if (st_id == 6) {
                LocationData6.push(dataTime); //lon for locate
                LocationData6.push(incomingLog.change.geo.longitude); //lon for locate
                LocationData6.push(incomingLog.change.geo.latitude); //lat for locate
                LocationData6.push(incomingLog.change.geo.altitude+altitudeOffset); //alt for locate
            }
            if (st_id == 7) {
                LocationData7.push(dataTime); //lon for locate
                LocationData7.push(incomingLog.change.geo.longitude); //lon for locate
                LocationData7.push(incomingLog.change.geo.latitude); //lat for locate
                LocationData7.push(incomingLog.change.geo.altitude+altitudeOffset); //alt for locate
            }
        }
    }
}

// * Function to check the ebikeVIN id here
function executeSTid(VIN) {
    if (VIN == "eBike20131126003c") {
        var id = 1;
    } else if (VIN == "eBike201209280029") {
        var id = 2;
    } else if (VIN == "eBike20131107003d") {
        var id = 3;
    } else if (VIN == "eBike201311270017") {
        var id = 4;
    } else if (VIN == "eBike20131127000a") {
        var id = 6;
    } else if (VIN == "eBike2013127000f") {
        var id = 7; //0E
    } else {
        var id = 0;
    }
    return id;
}

var CZMLWriter = function (ebikeName, LocationDataEbike, color_rgba, color_rgba_outline, labelOffset) {
    var czmlresult = [{
        "id": "document",
        "name": "Data_" + fileName,
        "version": "1.0",
        "clock": {
            "interval": startTime + "/" + endTime,
            "currentTime": startTime,
            "multiplier": 5,
            "range": "LOOP_STOP",
            "step": "SYSTEM_CLOCK_MULTIPLIER"
        }
    }, {
        "id": fileName + "_" + ebikeName,
        "name": ebikeName,
        "availability": startTime + "/" + endTime,
        "ellipsoid": {
            "radii": {
                "cartesian": [5, 5, 5]
            },
            "fill": true,
            "material": {
                "solidColor": {
                    "color": {
                        "rgba": color_rgba
                    }
                }
            }
        },
        "label": {
            "text": ebikeName,
            "font": "14pt sans-serif",
            "heightReference": "CLAMP_TO_GROUND",
            "showBackground": "true",
            "horizontalOrigin": "LEFT",
            "verticalOrigin": "BASELINE",
            "backgroundPadding": {
                "cartesian2": [20, 8]
            },
            "pixelOffset": {
                "cartesian2": [50, -50]
            },
            "backgroundColor": {
                "rgba": [
                    0,
                    0,
                    0,
                    125
                ]
            },
            "fillColor": {
                "rgba": color_rgba
            },
            "disableDepthTestDistance": 999999
        },
        "path": {
            "show": [{
                "interval": startTime + "/" + endTime,
                "boolean": true
            }
            ],
            "width": 4,
            "material": {
                "polylineOutline": {
                    "color": {
                        "rgba": color_rgba_outline
                    },
                    "outlineColor": {
                        "rgba": [0, 0, 0, 255]
                    },
                    "outlineWidth": 1
                }
            },
            "resolution": 1200,
            "leadTime": 0
        },
        "position": {
            "interpolationAlgorithm": "LINEAR",
            "interpolationDegree": 1,
            "epoch": startTime,
            "cartographicDegrees": LocationDataEbike
        }
    }];
    return czmlresult
}

init();