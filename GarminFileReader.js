// Require
var parse   = require('csv-parse');
const fs    = require('fs');
//-----------------------------------------Set up-----------------------------------------
var inputTCXfile = './Data/garminData/activity_2380867680_TCX.csv';
var inputGPXfile = './Data/garminData/activity_2380867680_GPX.csv';
var DataStreamID, outTCX, outGPX;
//--------------------------------------------------------------------------------------------
fs.readFile(inputTCXfile, function (err, outputTCX) {
    if (err) throw err;
    fs.readFile(inputGPXfile, function (err, outputGPX) {
        if (err) throw err;
        outGPX = outputGPX;
        outTCX = outputTCX;
        parse(outTCX, { comment: '#' }, function (err, outputTCX) {
            // Check the TCX file data
            console.log("Time : ", outputTCX[1][11]);
            console.log("DistanceMeters : ", outputTCX[1][12]);
            console.log("HeartRateBpm : ", outputTCX[1][13]);
            console.log("Speed : ", outputTCX[1][14]);
            parse(outGPX, { comment: '#' }, function (err, outputGPX) {
                // Check the TCX file data
                console.log("Temp C : ", outputGPX[1][11]);
                for (let i = 100; i < 105/*outputTCX.length size of the csv -1*/; i++) {
                    // TCX (More detailed and more rows than GPX)
                    console.log("=============Item :" + i + "=============");
                    console.log("Time : ", outputTCX[i][11]);
                    console.log("DistanceMeters : ", outputTCX[i][12]);     //* Only in TCX */
                    console.log("HeartRateBpm : ", outputTCX[i][13]);
                    console.log("Speed : ", outputTCX[i][14]);              //* Only in TCX */
                    console.log("Lat : ", outputTCX[i][15]);
                    console.log("Long : ", outputTCX[i][16]);
                    console.log("altitude : ", outputTCX[i][17]);
                    // GPX
                    console.log("Lat : ", outputGPX[i][7]);
                    console.log("Long : ", outputGPX[i][8]);
                    console.log("altitude : ", outputGPX[i][9]);
                    console.log("Time : ", outputGPX[i][10]);
                    console.log("Temp C : ", outputGPX[i][11]);             //* Only in GPX */
                    console.log("HeartRateBpm : ", outputGPX[i][12]);
                }

            });
        });
        
    });
});